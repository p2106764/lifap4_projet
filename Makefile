LIBS_HEADER := -ISFML/include -Iimgui/
SRCDIR := src
OBJDIR := obj

SRCFILES := $(wildcard $(SRCDIR)/*.cpp)
OBJFILES := $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(SRCFILES))

ifneq ($(filter $(OS),Windows windows Windows-NT win w),)
	LIBS := -lsfml-window -lsfml-audio -lsfml-graphics -lsfml-system -lopengl32
	LIBS_DIR := SFML/lib/windows
	LDFLAGS = -Wl,-rpath=$(LIBS_DIR),-subsystem,windows
   	TARGET = sfml-app.exe
	CLEANER = del /F /Q obj\*.o $(TARGET)
else
	LIBS := -lsfml-window -lsfml-audio -lsfml-graphics -lsfml-system -lGL
	LIBS_DIR := SFML/lib/linux
   	LDFLAGS = -Wl,-rpath=$(LIBS_DIR)
   	TARGET = sfml-app
	CLEANER = rm -f $(OBJFILES) $(TARGET)
endif

all: main

main: $(OBJFILES)
	g++ $^ -o $(TARGET) -L$(LIBS_DIR) $(LDFLAGS) $(LIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	g++ -c $< -o $@ $(LIBS_HEADER)

clean:
	$(CLEANER)
